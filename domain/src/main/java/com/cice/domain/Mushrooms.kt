package com.cice.domain

data class Mushrooms (
        var image : String? = null,
        var mushType : String? = null,
        var name : String? = null
    )