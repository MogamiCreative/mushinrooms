package com.cice.mushinrooms.extensions

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

//Esto hace que lo pueda utilizar desde cualquier layout tipo binding
@BindingAdapter("loadImage")
//Todos los imageView tienen el método loadImage
fun ImageView.loadImage(url : String) {
    Glide
        .with(this)
        .load(url)
        .centerCrop()
        .into(this)
}