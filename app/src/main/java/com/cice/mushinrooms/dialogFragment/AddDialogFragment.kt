package com.cice.mushinrooms.dialogFragment

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialogFragment
import com.cice.mushinrooms.R
import com.cice.mushinrooms.databinding.AddDialogFragmentBinding
import com.cice.mushinrooms.databinding.FragmentNewsBinding
import com.cice.mushinrooms.extensions.log
import com.cice.mushinrooms.extensions.setWidthPercent
import com.cice.mushinrooms.usescases.mushrooms.MushroomsFragment
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

//TODO: Encontrar nueva funcionalidad para realtime database

class AddDialogFragment : AppCompatDialogFragment() {

    private lateinit var binding : AddDialogFragmentBinding
    private lateinit var firebaseRef: DatabaseReference

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = AddDialogFragmentBinding.inflate(inflater, container, false)

        initDataBase()
        addNewmushroom()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //set dialog width
        setWidthPercent(90)
    }

    private fun addNewmushroom() {
        //Close Button
        binding.imageViewClose.setOnClickListener {
            dismiss()
        }

        //Accept Button
        binding.DialogCTA.setOnClickListener {
            //TODO: IF spinner Select type... -> Error
            //TODO: IF ELSE EditText está vacío -> Error

            //Get info
            val mushType = binding.spinnerVariety.selectedItem.toString()
            val name = binding.editName.text.toString()
            "el nombre es $name".log()
            "su tipo es $mushType".log()

            /*//Check
            if (binding.spinnerVariety.selectedItemPosition == 0) {
                Toast.makeText(context,"Select type of mushroom",Toast.LENGTH_SHORT).show()
            }

            if (name.isEmpty()){
                Toast.makeText(context,"Write new name",Toast.LENGTH_SHORT).show()
            }*/

            //Update info
            writeNewMushroom(mushType, name)
        }

    }

    private fun initDataBase() {
        val database = Firebase.database("https://mushinrooms-default-rtdb.europe-west1.firebasedatabase.app/")
        firebaseRef = database.getReference("mushrooms")
    }

    private fun writeNewMushroom(mushType: String, name: String) {
        val mushroom = mapOf<String,String>(
                "mushType" to mushType,
                "name" to name
        )
        firebaseRef.push().updateChildren(mushroom).addOnSuccessListener {

            binding.spinnerVariety.setSelection(0)
            binding.editName.text.clear()
            Toast.makeText(context,"New mushi added",Toast.LENGTH_SHORT).show()

        }.addOnFailureListener{

            Toast.makeText(context,"Failed to Update",Toast.LENGTH_SHORT).show()

        }
    }

}