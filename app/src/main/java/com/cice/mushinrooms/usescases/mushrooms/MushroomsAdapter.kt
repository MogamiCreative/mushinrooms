package com.cice.mushinrooms.usescases.mushrooms

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cice.domain.Mushrooms
import com.cice.mushinrooms.R
import com.cice.mushinrooms.databinding.ItemMushroomBinding
import com.cice.mushinrooms.extensions.log

//TODO - Terminar de implementar Recycler
//TODO - Añadir post con click al recycler

class MushroomsAdapter(var mushroomList: ArrayList<Mushrooms>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int = mushroomList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemMushroomBinding = DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_mushroom,
                parent,
                false
        )
        return MushroomsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MushroomsViewHolder).onBind(mushroomList[position])
        "onBindViewHolder".log()
    }

    //ViewHolder
    private class MushroomsViewHolder (val binding: ItemMushroomBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(mushroomList: Mushrooms) {
            binding.mushroom = mushroomList
            binding.executePendingBindings()
            "ViewHolder".log()
        }
    }

}