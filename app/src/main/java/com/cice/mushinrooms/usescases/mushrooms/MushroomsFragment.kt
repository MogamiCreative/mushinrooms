package com.cice.mushinrooms.usescases.mushrooms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.cice.domain.Mushrooms
import com.cice.mushinrooms.databinding.FragmentMushroomsBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.snapshot.ChildKey
import com.google.firebase.ktx.Firebase

class MushroomsFragment : Fragment() {

    private lateinit var viewModel: MushroomsViewModel
    private lateinit var binding: FragmentMushroomsBinding
    private lateinit var mushroomsList : ArrayList<Mushrooms>
    private lateinit var firebaseRef: DatabaseReference

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        //Inflar vista desde binding
        binding = FragmentMushroomsBinding.inflate(inflater, container, false)

        //ViewModel
        viewModel = ViewModelProvider(this).get(MushroomsViewModel::class.java)

        //Initialized
        mushroomsList = arrayListOf()

        //Recycler
        val recyclerView = binding.recyclerMushrooms
        val layoutManager = GridLayoutManager(context, 2)
        recyclerView.layoutManager = layoutManager

        //Database
        initDataBase()
        getMushrooms()

        return binding.root
    }

    //TODO: Meter auth con google
    //TODO: Meter permisos en realtime database para que no se pueda reescribir esta parte

    private fun initDataBase() {
        val database = Firebase.database("https://mushinrooms-default-rtdb.europe-west1.firebasedatabase.app/")
        firebaseRef = database.getReference("mushrooms")
    }

    private fun getMushrooms() {
        firebaseRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (mushroomsSnapshot in snapshot.children) {
                        val mushroom = mushroomsSnapshot.getValue(Mushrooms::class.java)
                        mushroomsList.add(mushroom!!)
                    }
                    binding.recyclerMushrooms.adapter = MushroomsAdapter(mushroomsList)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(
                    context,
                    error.message,
                    Toast.LENGTH_SHORT
                ).show()
            }

        })

    }


        /*fun supermetodo(entero : Int) : Int {

            if (entero < 8)
                return 0

                return 100
        }*/

}