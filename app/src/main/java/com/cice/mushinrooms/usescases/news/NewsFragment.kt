package com.cice.mushinrooms.usescases.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.cice.mushinrooms.R
import com.cice.mushinrooms.databinding.FragmentMushroomsBinding
import com.cice.mushinrooms.databinding.FragmentNewsBinding
import com.cice.mushinrooms.usescases.mushrooms.MushroomsViewModel

class NewsFragment : Fragment() {

    lateinit var binding : FragmentNewsBinding
    lateinit var viewModel : NewsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Inflar vista desde binding
        binding = FragmentNewsBinding.inflate(inflater, container, false)

        //ViewModel
        viewModel = ViewModelProvider(this).get(NewsViewModel::class.java)

        binding.lifecycleOwner = this

        //viewModel.readEditText = ::completeTextView

        binding.viewModel = viewModel

        return binding.root
    }

    /*private fun completeTextView() {
        var text = binding.editName.text.toString()

        //binding.textNews.text = text
        viewModel.textMutable.value = text
    }*/

}