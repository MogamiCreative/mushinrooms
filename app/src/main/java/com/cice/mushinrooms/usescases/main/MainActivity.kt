package com.cice.mushinrooms.usescases.main

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.cice.mushinrooms.R
import com.cice.mushinrooms.dialogFragment.AddDialogFragment
import com.cice.mushinrooms.extensions.log
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {

    private lateinit var firebaseRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Start Methods
        initFragment()
        onCLickShowDialog()
    }

    private fun initFragment() {
        val navController = findNavController(R.id.fragmentMain)
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.mainMenuBottom)

        bottomNavigationView.setupWithNavController(navController)
    }

    private fun onCLickShowDialog() {
        "onCLickAddMushroom".log()

        val addButtonMenu = findViewById<ImageView>(R.id.addButton)

        addButtonMenu.setOnClickListener{
            var dialogFragment = AddDialogFragment()

            dialogFragment.show(supportFragmentManager, "customDialog")
        }

    }
}